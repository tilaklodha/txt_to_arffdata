package txt_to_arffdata;
import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.Map.Entry;

import weka.classifiers.*;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.*;
import weka.core.*;



public class text_to_arff{
	
	static HashMap<String, Integer> attribute = new HashMap<String, Integer>();
    
    int attribnum=1; // number of attributes 
	
	static Scanner scanner=new Scanner(System.in);
	
	
	
	
		public static void main(String[] args) throws IOException {
				        
	        
			text_to_arff object= new text_to_arff();
			
			attribute= object.interrogative();
			attribute= object.imperative();
			attribute= object.declarative();
			attribute=object.attributefilegenerate();
			
			System.out.println(attribute);
			//System.out.println(attribute.get("cancel"));
			//System.out.println(attribute.size());
			deletefile();
			trainfile(attribute);   
			duplicateFile();
			data(attribute);
			
			//int [] wordvector= new int[attribute.size()];
			//Arrays.fill(wordvector, 0);
			//System.out.println(wordvector[256]);
			
			datamodel2(attribute);
			trainingInWeka();
			trainingInWekamodel2();
		}	
		
		
		
		
		
		
		
		
		
		

		
		
		public HashMap<String,Integer> interrogative() {
			
			//--------Interrogative attributes into HashMap	        
			try {
				scanner = new Scanner(new FileReader("./src/resources/sentences/interrogative.txt"));
			 // file input for interrogative sentences
		
	        

	        while (scanner.hasNextLine()) {
	        	String sentence= scanner.nextLine();
	        	int k= sentence.indexOf("$$$");
		    	String gesture= sentence.substring(k+4, sentence.length());
				sentence=sentence.substring(0, k);
	        
	        	sentence.replaceAll("[-+.^:,:'/]", "");	        	
	            String[] columns = sentence.split("((?<=;)|(?=;))| |((?<=\\?)|(?=\\?))|((?<=//.)|(?=//.))");
	            int numberofwords= columns.length;
	            for(int i=0;i<numberofwords;i++){
	            	if(attribute.containsKey(columns[i])){
	            		
	            	}
	            	else{
	            		attribute.put(columns[i], attribnum);
	            		attribnum++;
	            	}
	            }
	            
	            
	            
	        }
	        
	        scanner.close();
	        
	        
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	//--------end of Interrogative Sentences
			return attribute;
			
			
			
			
		}
		
		
		public HashMap<String,Integer> imperative() {
			//--------Imperative attributes into HashMap		        
	        try {
				scanner = new Scanner(new FileReader("./src/resources/sentences/imperative.txt"));
						// file input for imperative sentences
	        
	        //attribnum=1;
	        while (scanner.hasNextLine()) {
	        	String sentence1= scanner. nextLine();
	        	int k= sentence1.indexOf("$$$");
		    	String gesture= sentence1.substring(k+4, sentence1.length());
				sentence1=sentence1.substring(0, k);
	        	sentence1.replaceAll("[-+.^:,:'/]", "");	        	
	            String[] columns = sentence1.split("((?<=;)|(?=;))| |((?<=\\?)|(?=\\?))");
	            int numberofwords= columns.length;
	            for(int i=0;i<numberofwords;i++){
	            	if(attribute.containsKey(columns[i])){
	            		
	            	}
	            	else{
	            		attribute.put(columns[i], attribnum);
	            		attribnum++;
	            	}
	            }
	            
	        }
	             
	        scanner.close();
//----------- End of Imperative Sentences
	        } catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return attribute;
			
			
			
		}
		
		
		
		
		
		
		public HashMap<String,Integer> declarative() {
			//--------Daclarative attributes into HashMap		        
	        try {
				scanner = new Scanner(new FileReader("./src/resources/sentences/declarative.txt"));
					// file input for declarative sentences
	        
	        //attribnum=1;
	        while (scanner.hasNextLine()) {
	        	String sentence2= scanner. nextLine();
	        	int k= sentence2.indexOf("$$$");
		    	String gesture= sentence2.substring(k+4, sentence2.length());
				sentence2=sentence2.substring(0, k);
	        	sentence2.replaceAll("[-+.^:,:'/]", "");	        	
	            String[] columns = sentence2.split("((?<=;)|(?=;))| |((?<=\\?)|(?=\\?))");
	            int numberofwords= columns.length;
	            for(int i=0;i<numberofwords;i++){
	            	if(attribute.containsKey(columns[i])){
	            		
	            	}
	            	else{
	            		attribute.put(columns[i], attribnum);
	            		attribnum++;
	            	}
	            }
	            
	        }
	             
	        scanner.close();
	        
	        } catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	        //System.out.println(attribnum);
			return attribute;
			
			
			

		}
		
		
		public HashMap<String,Integer> attributefilegenerate() {
			
			//----------Merging of Different Attributes in one HashMap	        
	        //attribute.putAll(mapinterrogative);
	        //attribute.putAll(mapimperative);
	        //attribute.putAll(mapdeclarative);
	        
	        //System.out.println(attribute);
	        //System.out.println(attribute.keySet());
	        //System.out.println(attribute.size());  		// Size of attributes
	        
	        FileWriter fstream;
	        BufferedWriter out;
	        // then, define how many records we want to print to the file
	        int recordsToPrint = attribute.size();
	        // create your filewriter and bufferedreader
	        try {
				fstream = new FileWriter("./src/resources/attributes.arff");
			 // Output file of Attributes
	        out = new BufferedWriter(fstream);

	        // initialize the record count
	        int count = 0;

	        // create your iterator for your map
	        Iterator<Entry<String, Integer>> it = attribute.entrySet().iterator();

	        // then use the iterator to loop through the map, stopping when we reach the
	        // last record in the map or when we have printed enough records
	        while (it.hasNext() && count < recordsToPrint) {

	            // the key/value pair is stored here in pairs
	            Map.Entry<String, Integer> pairs = it.next();
	            //System.out.println(pairs.getValue());

	            // since you only want the value, we only care about pairs.getValue(), which is written to out
	            if(pairs.getKey()!=" "){
	            	out.write(pairs.getKey() + " " + "\n");
	            }
	            out.flush();
	            // increment the record count once we have printed to the file
	            count++;
	        }
	        // lastly, close the file and end
	        out.close();
			
			
	        } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return attribute; 
			
		}
		
		
		
		public static void trainfile(HashMap<String, Integer> train) {
			
			Set<Entry<String, Integer>> set = train.entrySet();
	        List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(set);
	        Collections.sort( list, new Comparator<Map.Entry<String, Integer>>()
	        {
	            public int compare( Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2 )
	            {
	                return (o1.getValue()).compareTo( o2.getValue() );
	            }
	        } );
	        
	        FileWriter fstream;
	        BufferedWriter out;
	        try {
				fstream = new FileWriter("./src/resources/train.arff");
			  // Output file of Attributes
	        out = new BufferedWriter(fstream);
	        out.write("@relation 'C__Users_hp1_Desktop_intent-identification-weka.filters.unsupervised.attribute.StringToWordVector-R1-W1000-prune-rate-1.0-N0-stemmerweka.core.stemmers.NullStemmer-M1-tokenizerweka.core.tokenizers.WordTokenizer -delimiters \" \\r\\n\\.,;:\\\'\\\"()\"' \n \n");
	        for(Map.Entry<String, Integer> entry:list){
	        	
	            //System.out.println(entry.getKey());
	            if(entry.getKey().equalsIgnoreCase(" ") || entry.getKey().equalsIgnoreCase("\t") || entry.getKey().equalsIgnoreCase("\n") ){
	            	out.write("@attribute " + "\"" + entry.getKey() + "\""+ " numeric\n" );
	            	System.out.println(entry.getKey());
	            }
	            else{
	            	
	            	out.write("@attribute \'" + entry.getKey() + "\' numeric\n" );
	            	System.out.println(entry.getKey());
	            }
	        }
	        
	        out.close();
	        } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
		}
		
		
		
		
		public static void data(HashMap<String, Integer> train){
			File folder = new File("./src/resources/sentences/");
			File[] listOfFiles = folder.listFiles();
			int [] wordvector= new int[attribute.size()];
			
			FileWriter fstream;
			try {
				fstream = new FileWriter("./src/resources/train.arff", true);
			
				BufferedWriter out;
				out = new BufferedWriter(fstream);
	        
				out.write("@attribute @@class@@ {imperative, interrogative, declarative}\n \n");
		        out.write("@data"+ "\n\n");
			for (int i = 0; i < listOfFiles.length; i++){
							
				//System.out.println(listOfFiles[i].getName().replaceFirst("[.][^.]+$", "")+ "\n");  
				scanner = new Scanner (new FileReader("./src/resources/sentences/"+listOfFiles[i].getName()));
				//System.out.println(listOfFiles[i].getName());
				while (scanner.hasNextLine()) {
					
					
		        	String sentence= scanner.nextLine();
		        	//System.out.println(sentence);
		        	//sentence.replaceAll("[-+.^:,:'/]", "");	        	
		            String[] columns = sentence.split("((?<=;)|(?=;))| |((?<=\\?)|(?=\\?))|((?<=//.)|(?=//.))");
		            int numberofwords= columns.length;
		            //System.out.println(numberofwords);
		            for(int k=0;k<numberofwords;k++){
		            	//System.out.print(attribute.containsKey(columns[k]));
		            	if(attribute.containsKey(columns[k])){
		            		wordvector[attribute.get(columns[k])-1]=1;
		            		//System.out.println(attribute.get(columns[k]));
		            	}
		            	else{
		            		
		            	}
		            }
		            //System.out.println(sentence);
		            
		            for(int l=0;l<wordvector.length; l++){
		            	//System.out.print(wordvector[l]+",");
		            	out.write(wordvector[l]+",");
		            }
		            //System.out.print(listOfFiles[i].getName().replaceFirst("[.][^.]+$", "\n"));
		            out.write(listOfFiles[i].getName().replaceFirst("[.][^.]+$", "\n")	);
		            out.flush();
		            Arrays.fill(wordvector, 0);
		            
		        }
				
				
			}
			out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		
		
		public static void trainingInWeka(){
			BufferedReader file= null;
			
			try {
				file= new BufferedReader(new FileReader("./src/resources/train.arff"));
			

				Instances train= new Instances(file);
			train.setClassIndex(train.numAttributes()-1);
			
			file.close();
			
			SMO nb = new SMO();
			nb.buildClassifier(train);
	        
			Evaluation eval = new Evaluation(train);
			Random rand = new Random(1);
			eval.crossValidateModel(nb, train, 50, rand);
			System.out.println(eval.toSummaryString("\nResults\n======\n", true));
			System.out.println(eval.fMeasure(1) + " " + eval.precision(1) + " " + eval.recall(1));
			System.out.println(eval.toMatrixString());
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		public static void duplicateFile(){
			
			
			//PrintWriter writer = null;
			
			File source = new File("./src/resources/train.arff");
			File dest = new File("./src/resources/trainmodel2.arff");

			try {
				
				Files.copy(source.toPath(), dest.toPath());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  
		}
		
		
		
		public static void datamodel2(HashMap<String, Integer> train){
			File folder = new File("./src/resources/sentences/");
			File[] listOfFiles = folder.listFiles();
			int [] wordvector= new int[attribute.size()];
			
			
			try {
				
				FileWriter fstream;
				fstream = new FileWriter("./src/resources/trainmodel2.arff", true);
			
				BufferedWriter out;
				out = new BufferedWriter(fstream);
				out.write("@attribute @class@ {imperative, declarative, interrogative} \n");
				out.write("@attribute @@class@@ {request, demand}\n \n");
		        out.write("@data"+ "\n\n");
	        
			for (int i = 0; i < listOfFiles.length; i++){
							
				//System.out.println(listOfFiles[i].getName().replaceFirst("[.][^.]+$", "")+ "\n");  
				scanner = new Scanner (new FileReader("./src/resources/sentences/"+listOfFiles[i].getName()));
				//System.out.println(listOfFiles[i].getName());
				while (scanner.hasNextLine()) {
					
					
		        	String sentence= scanner.nextLine();
		        	int k= sentence.indexOf("$$$");
			    	String gesture= sentence.substring(k+4, sentence.length());
					sentence=sentence.substring(0, k);
		        	//System.out.println(sentence);
		        	//sentence.replaceAll("[-+.^:,:'/]", "");	        	
		            String[] columns = sentence.split("((?<=;)|(?=;))| |((?<=\\?)|(?=\\?))|((?<=//.)|(?=//.))");
		            int numberofwords= columns.length;
		            //System.out.println(numberofwords);
		            for(int j=0;j<numberofwords;j++){
		            	//System.out.print(attribute.containsKey(columns[k]));
		            	if(attribute.containsKey(columns[j])){
		            		wordvector[attribute.get(columns[j])-1]=1;
		            		//System.out.println(attribute.get(columns[k]));
		            	}
		            	else{
		            		
		            	}
		            }
		            //System.out.println(sentence);
		            
		            for(int l=0;l<wordvector.length; l++){
		            	//System.out.print(wordvector[l]+",");
		            	out.write(wordvector[l]+",");
		            }
		            //System.out.print(listOfFiles[i].getName().replaceFirst("[.][^.]+$", "\n"));
		            out.write(listOfFiles[i].getName().replaceFirst("[.][^.]+$", "")+ ","+ gesture + "\n");
		            //out.write(","+ gesture);
		            out.flush();
		            Arrays.fill(wordvector, 0);
		            
		        }
				
				
			}
			out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		public static void deletefile(){
			try{
	    		
	    		File file = new File("./src/resources/trainmodel2.arff");
	        	
	    		if(file.delete()){
	    			System.out.println(file.getName() + " is deleted!");
	    		}else{
	    			System.out.println("Delete operation is failed.");
	    		}
	    	   
	    	}catch(Exception e){
	    		
	    		e.printStackTrace();
	    		
	    	}
	    	
		}
		
		
		public static void trainingInWekamodel2(){
			BufferedReader file= null;
			
			try {
				file= new BufferedReader(new FileReader("./src/resources/trainmodel2.arff"));
			

				Instances train= new Instances(file);
			train.setClassIndex(train.numAttributes()-1);
			
			file.close();
			
			SMO nb = new SMO();
			nb.buildClassifier(train);
	        
			Evaluation eval = new Evaluation(train);
			Random rand = new Random(1);
			eval.crossValidateModel(nb, train, 50, rand);
			System.out.println(eval.toSummaryString("\nResults\n======\n", true));
			System.out.println(eval.fMeasure(1) + " " + eval.precision(1) + " " + eval.recall(1));
			System.out.println(eval.toMatrixString());
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
}



